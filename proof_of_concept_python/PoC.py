import pin
import statics
from web3 import Web3
import json
from kms.kms import get_eth_address, sign_transaction

web3 = Web3(Web3.HTTPProvider(statics.INFURA_URI))
web3.eth.default_account = web3.toChecksumAddress(statics.PUBLIC_KEY)
print(web3.toChecksumAddress(web3.toChecksumAddress(statics.PUBLIC_KEY)))


contract_abi_file = open(statics.CONTRACT_ABI, 'r')
contract_loaded_json = json.load(contract_abi_file)
abi, bytecode = contract_loaded_json['abi'], contract_loaded_json['bytecode']


def deploy_contract(KMS_KEY_ID=statics.KMS_KEY_ID):
    contract = web3.eth.contract(abi=abi, bytecode=bytecode)
    tx = contract.constructor().buildTransaction({
        'from': web3.eth.default_account,
        'nonce': web3.eth.getTransactionCount(web3.eth.default_account),
        'gas': contract.constructor().estimateGas(),
        'gasPrice': web3.eth.gas_price
    })

    signed = web3.eth.account.sign_transaction(tx, statics.PRIVATE_KEY)

    tx_hash = web3.eth.sendRawTransaction(signed.rawTransaction)
    return web3.eth.wait_for_transaction_receipt(tx_hash).contractAddress


def mint_nft(token_uri, recipient=get_eth_address(statics.KMS_KEY_ID),
             nonce=web3.eth.getTransactionCount(web3.toChecksumAddress(statics.PUBLIC_KEY)),
             gas=None):
    contract = web3.eth.contract(address=statics.CONTRACT_ADDRESS, abi=abi)

    tx = contract.functions.mintNFT(recipient, token_uri).buildTransaction(
        {
            'nonce': nonce,
            'gas': 500000 if not gas else gas,
            'gasPrice': web3.eth.gas_price
        }
    )

    signed_tx = web3.eth.account.sign_transaction(tx, statics.PRIVATE_KEY)
    print(signed_tx)
    print(web3.eth.send_raw_transaction(signed_tx.rawTransaction).hex())


def mint_nfts(recipients: list, uris: list, nonce=web3.eth.getTransactionCount(web3.toChecksumAddress(get_eth_address(statics.KMS_KEY_ID))),
              gas=None):

    if len(recipients) != len(uris):
        raise AssertionError("Amount of recipients has to be equal to amount of uris!")
    contract = web3.eth.contract(address=statics.CONTRACT_ADDRESS, abi=abi)

    tx = contract.functions.mintNFTs(recipients, uris).buildTransaction(
        {
            'nonce': nonce,
            'gas': 500000*len(recipients) if not gas else gas,
            'gasPrice': web3.eth.gas_price
        }
    )
    signed_tx = sign_transaction(tx, statics.KMS_KEY_ID)
    print(signed_tx)
    print(web3.eth.send_raw_transaction(signed_tx.rawTransaction).hex())


if __name__ == '__main__':
    print(web3.eth.getBalance(web3.eth.default_account))
    # print(deploy_contract())
    # print(abi)
    # mint_nfts(['0xB93C7Ca7A742335BfA4aA1830cEAEd829f077068', '0x34020209A8b882118141727E271E4379f3F2BF20'],
    #           [pin.upload_image_generate_uri('obrazek1', 'o1.png', description='obrazek 1'),
    #            pin.upload_image_generate_uri('obrazek2', 'o2.png', description='obrazek 2')])
    mint_nft(pin.upload_image_generate_uri('ticketQR', 'qr.png', description='link', attributes={'stage': 1}), '0x175d37E4093340d3EEa8BFB7Cff27876B156Cd7F')


