import json
import requests
import statics


def upload_file(name: str, file_path: (str or bytes)) -> dict:
    files = {
        name: open(file_path, 'rb').read() if isinstance(file_path, str) else file_path
    }
    response = requests.post(statics.IPFS_URI + '/add', files=files, auth=(statics.INFURA_KEY, statics.INFURA_SECRET))
    return response.json()


def unpin_file(hash: str) -> dict:
    params = {
        'arg': hash
    }
    response = requests.post(statics.IPFS_URI+'/pin/rm', params=params,
                             auth=(statics.INFURA_KEY, statics.INFURA_SECRET))
    return response.json()


def upload_image_generate_uri(name: str, file_path: (str or bytes), **kwargs) -> str:
    """
    Uploads image to ipfs, then creates a json linking to the image
    :param name: name of the image
    :param file_path: bytes of image file or path to it
    :param kwargs: arguments to be added to json metadata
    :return: uri of the json with linked image
    """
    upload_response = upload_file(name, file_path)['Hash']
    kwargs['image'] = statics.IPFS+upload_response
    kwargs['name'] = name
    return statics.IPFS+upload_file(name+"_meta", bytes(json.dumps(kwargs), 'UTF-8'))['Hash']


