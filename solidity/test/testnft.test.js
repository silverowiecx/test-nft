const { ethers } = require("hardhat");
const assert = require('assert');

let accounts;
let test_nft;

const TEST_IPFS_URI = 'https://gateway.pinata.cloud/ipfs/QmapzXNY2wYEdyvhZpqayMu7FF8TJe6QTo16ZXeRAaaGvr';

beforeEach(async () => {

    accounts = await ethers.getSigners(3);

    const TestNFT = await ethers.getContractFactory("TestNFT");
    test_nft = await TestNFT.deploy();
    await test_nft.deployed();
});



describe('TestNFT', () => {
    it('deploys a contract', () => {
        assert.ok(test_nft.address);
    });


    it('mints nft', async () => {
        await test_nft.mintNFT(accounts[1].address, TEST_IPFS_URI);
        assert(await test_nft.balanceOf(accounts[1].address)==(1));
    });

    it('mints multiple nfts', async () => {
        await test_nft.mintNFTs([accounts[1].address, accounts[1].address], [TEST_IPFS_URI, TEST_IPFS_URI]);
        assert(await test_nft.balanceOf(accounts[1].address)==(2));
    });

    it('does not exceed cap', async() =>{
        for (var i=0; i<5; i++){
            await test_nft.mintNFTs([accounts[1].address, accounts[1].address], [TEST_IPFS_URI, TEST_IPFS_URI]);
        }
        try{
            await test_nft.mintNFT(accounts[1].address, TEST_IPFS_URI);
            assert(0==1);
        } catch(Error){
            assert.ok(true, "Contract is throwing an error.");
        }
        
    });
});