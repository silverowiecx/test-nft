//Contract based on [https://docs.openzeppelin.com/contracts/3.x/erc721](https://docs.openzeppelin.com/contracts/3.x/erc721)
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

contract TestNFT is ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;
    uint16 public HARD_CAP = 10;

    constructor() public ERC721("TestNFT", "NFT") {}

    function mintNFT(address recipient, string memory tokenURI)
        public onlyOwner
        returns (uint256)
    {
        _tokenIds.increment();

        uint256 newItemId = _tokenIds.current();
        require(newItemId <= HARD_CAP, "Number of minted NFTs cap reached!");
        _mint(recipient, newItemId);
        _setTokenURI(newItemId, tokenURI);

        return newItemId;
    }
    
    function mintNFTs(address[] memory _addresses, string[] memory _URIs) 
    public onlyOwner
    returns (uint256[] memory)
    {
        require(_addresses.length == _URIs.length, "Improper amount of addresses and URIs are provided!");
        require(_tokenIds.current()+_addresses.length <= HARD_CAP, "Number of minted NFTs cap reached!");
        uint256[] memory _results = new uint256[](_addresses.length);
        
        for(uint8 i = 0; i < _addresses.length; i++)
        {
            _results[i] = (mintNFT(_addresses[i], _URIs[i]));
        }
        return _results;
    }

}

